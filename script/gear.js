function getDomainByUrl(url) {
	var match = url.match(/:\/\/(.[^/]+)/);
	if (typeof match == 'undefined' || match == null) {
		return null;
	}
	return match[1];
}

function getMainDomainByDomain(domain) {
	var ds = domain.split('.');
	var num = ds.length;
	if (num >= 3) {
		if (ds[num - 2] == 'com') {
			return ds[num - 3] + '.' + ds[num - 2] + '.' + ds[num - 1];
		} else {
			return ds[num - 2] + '.' + ds[num - 1];
		}
	} else {
		return domain;
	}
}

function getMainDomainByUrl(url) {
	var domain = getDomainByUrl(url);
	return getMainDomainByDomain(domain);
}

function getProtocolByUrl(url) {
	var idx = url.indexOf(':');
	if (idx < 0) {
		return null;
	}
	return url.substring(0, idx);
}

