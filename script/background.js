(function() {

console.log('background start');

function reportSitesData() {
	chrome.storage.local.get(null, function(items) {
		var sitis = [];
		$.each(items, function(key, value) {
			if (key.indexOf('site') < 0) {
				return;
			}
			var siti = JSON.parse(value);
			sitis[sitis.length] = siti;
		});
		if (sitis.length > 0) {
			sitis.sort(sitiSortDescTime);
			reportSiteData(sitis[0]);
		}
	});
}

function clearTabsInfo(max) {
	max = max | 1000;
	for (var i = 0; i < max; i++) {
		var key = "tab_" + i;
		chrome.storage.local.remove(key);
	}
}

function startViewPage(tabi) {
	var key = 'tab_' + tabi.tabId;
	var ti = {};
	ti[key] = JSON.stringify(tabi);
	chrome.storage.local.set(ti);
}

function stopViewPage(tabi) {
    var etime = new Date().getTime();
    var msec = etime - tabi.stime;
    /*
	console.log('close, id: ' + tabi.tabId
            + ', protocol: ' + tabi.protocl
            + ', site: ' + tabi.site
			+ ', cate: ' + tabi.cate);
	*/
	var key = "tab_" + tabi.tabId;
	chrome.storage.local.remove(key);
	
	key = "site_" + tabi.site;
	chrome.storage.local.get(key, function(item) {
		var siti = { };
		if (!item[key]) {
			siti.site = tabi.site;
			siti.msec = msec;
			siti.cate = tabi.cate;
		} else {
			siti = JSON.parse(item[key]);
			if (siti.cate == 'unknown') {
				siti.cate = findSiteCate(tabi.site);
			}
            if (siti.msec) {
			    siti.msec = siti.msec + msec;
            } else {
			    siti.msec = msec;
            }
		}
		var si = {};
		si[key] = JSON.stringify(siti);
		chrome.storage.local.set(si);
	});
}

function handlePageView(tab) {
	var tabi= { };
	tabi.url = tab.url;
    tabi.tabId = tab.id;
	tabi.site = getDomainByUrl(tab.url);
	tabi.domain = getMainDomainByDomain(tabi.site);
	tabi.protocol = getProtocolByUrl(tab.url);
    tabi.stime = new Date().getTime();
	tabi.cate = findSiteCate(tabi.site);
	/*
	console.log('open, id: ' + tabi.tabId
			+ ', url: ' + tabi.url
            + ', protocol: ' + tabi.protocl
            + ', domain: ' + tabi.domain
			+ ', cate: ' + tabi.cate);
	*/
	
	if (tabi.protocol.indexOf('chrome') >= 0) {
		return;
	}

    var key = "tab_" + tab.id;
	chrome.storage.local.get(key, function(item) {
		var tabid = { };
		if (item[key]) { // found
			tabid = JSON.parse(item[key]);
			if (tabid.url != tabi.url) {
				stopViewPage(tabid);
				startViewPage(tabi);
			}
		} else {
			startViewPage(tabi);
		}
    });
}

function urlUpdatedListener(tab) {
	handlePageView(tab);
}

function pageFinishListener(tab) {
	var protocol = getProtocolByUrl(tab.url);
	if (protocol.indexOf('chrome') >= 0) {
		return;
	}
}

function beforeRequestListener(details) {
	// if (details.tabId < 0) {
	var url = details.url;
	var site = getSiteEntry(url);
	// console.debug("%s, %s", url, JSON.stringify(site));
	if ((site) && ($.isFunction(site["filter"]))) {
		var ret = site.filter({ "url": url });
		/*
		if (!site.accept()) {
			console.debug("stop url: %s", url);
			// var nurl = "";
			return { "cancel": true }; 
		}
		*/
		if (ret.cancel) {
			console.debug("stop url: %s", url);
			return { "cancel": true }; 
		}
		if (ret.redirectUrl) {
			console.debug("redirect, oldurl: %s, newurl: %s", url, ret.redirectUrl);
			return { "redirectUrl": ret.redirectUrl };
		}
	}
	// }
}

function tabCloseListener(tabi) {
	stopViewPage(tabi);
}

function tabCreatedListener(tab) {
	// console.log('create, url: ' + tab.url);
}

function tabUpdatedListener(tabId, event, tab) {
	// console.log('update, status: ' + event.status + ', url: ' + tab.url);
	if (event.status == 'loading') {
		urlUpdatedListener(tab);
	} else if (event.status == 'complete') {
		pageFinishListener(tab);
	}
}

function tabRemovedListener(tabId, info) {
    var key = "tab_" + tabId;
    chrome.storage.local.get(key, function(item) {
		if (!item[key]) {
            return;
        }
        var tabi = JSON.parse(item[key]);
        console.debug(tabi);
        tabCloseListener(tabi);
    });
}

function windowCreatedListener(window) {
	reportSitesData();
}

function windowRemovedListener(winId) {
	clearTabsInfo();
}

(function() {
	var manifest = chrome.runtime.getManifest();
	console.debug(JSON.stringify(manifest));
	
	chrome.webRequest.onBeforeRequest.addListener(
			beforeRequestListener, 
			{ urls: [ "<all_urls>" ] },
			[ "blocking" ]
	);
	
	chrome.tabs.onCreated.addListener(tabCreatedListener);
	chrome.tabs.onUpdated.addListener(tabUpdatedListener);
	chrome.tabs.onRemoved.addListener(tabRemovedListener);
	
	chrome.windows.onCreated.addListener(windowCreatedListener);
	chrome.windows.onRemoved.addListener(windowRemovedListener);
})();

})();
