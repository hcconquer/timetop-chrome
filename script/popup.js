$(function() {

console.log('popup start');

$('#clear_stats').text(chrome.i18n.getMessage('clear_stats'));
$('#stats_site').text(chrome.i18n.getMessage('stats_site'));
$('#stats_time').text(chrome.i18n.getMessage('stats_time'));
$('#stats_perc').text(chrome.i18n.getMessage('stats_perc'));
$('#stats_cate').text(chrome.i18n.getMessage('stats_cate'));

chrome.storage.local.get(null, function(items) {
	var sitis = [];
	var totalMsec = 0;
	$.each(items, function(key, value) {
		if (key.indexOf('site') < 0) {
			return;
		}
		var siti = JSON.parse(value);
		sitis[sitis.length] = siti;
		totalMsec = totalMsec + siti.msec;
	});
	sitis.sort(sitiSortDescTime);
	// console.debug(sitis);
	
	var stats_items = $("#stats_items");
	$.each(sitis, function(idx, siti) {
        var perc = siti.msec * 100 / totalMsec;
        if ((idx >= 20) && (perc < 1)) {
            return false;
        }
		var stats_item = $("<tr></tr>");
		// var stats_item_site = $("<td></td>").append($("<a target='_blank'></a>").text(siti.site).attr("href", siti.site));
		var stats_item_site = $("<td></td>").text(siti.site);
		var second = (siti.msec / 1000).toFixed(0);
		var minute = (second / 60).toFixed(0);
		second = second % 60;
		var hour = (minute / 60).toFixed(0);
		minute = minute % 60;
		// var stats_item_time = $("<td></td>").text((siti.msec / 1000).toFixed(0) + 's');
		var stats_item_time = $("<td></td>").text(chrome.i18n.getMessage('stats_time_format_hhmmss', [ hour, minute, second ]));
		var stats_item_perc = $('<td></td>').text(perc.toFixed(2) + '%');
		var stats_item_cate = $('<td></td>').text(siti.cate);
		stats_item.append(stats_item_site).append(stats_item_time).append(stats_item_perc).append(stats_item_cate);
		stats_items.append(stats_item);
	});
});

$("#clear_stats").click(function() {
	chrome.storage.local.get(null, function(items) {
		$.each(items, function(key, value) {
			chrome.storage.local.remove(key);
		});
		var stats_items = $("#stats_items");
		stats_items.empty();
	});
});

});

