$(function() {

console.log("timetop content start");

function requestListener(request, sender, sendResponse) {
	var resp = { 
		"action": request.action,
		"result": 0
	};
	if (request.action == 'test') {
		sendResponse(resp);
	}
}

(function() {
	var url = window.location.href;
	var site = getSiteEntry(url);
	if (site != null) {
		if ($.isFunction(site["relieve"])) {
			site.relieve();
		}
	}
})();

chrome.extension.onRequest.addListener(requestListener);

});
