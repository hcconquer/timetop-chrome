var g_sites = [
    { "site": "zhihu.com", "cata": "knowledge" },
    { "site": "jd.com", "cata": "E-Commerce" },
    { "site": "mail.10086.cn", "cata": "office" },
    { "site": "shanbay.com", "cata": "knowledge" },
    { "site": "stackoverflow.com", "cata": "IT" },
    { "site": "cnblogs.com", "cata": "IT" },
    { "site": "eastmoney.com", "cata": "finance" },
	{ "site": "zone.qq.com", "cata": "social" },
	{ "site": "renren.com", "cata": "social" },
	{ "site": "weibo.com", "cata": "social" },
	{ "site": "facebook.com", "cata": "social" },
	{ "site": "douban.com", "cata": "social" },
	{ "site": "126.net", "cata": "news", "relieve": function() {
	}, "filter": function(params) {
		var url = params.url;
		var ret = {};
		if (url) {
			if ((url.indexOf("adbox") > 0) || (url.indexOf("dlbox") > 0)) {
				ret.cancel = true;
			}
//			if (url.indexOf("jquery") > 0) {
//				ret.redirectUrl = chrome.extension.getURL("script/jquery.min.js");
//			}
		}
		return ret;
	} },
	{ "site": "163.com", "cata": "news", "relieve": function() {
	}, "filter": function(params) {
		// console.debug(JSON.stringify(params));
		return {};
	} },
	{ "site": "cdn.staticfile.org", "cata": "toos", "filter": function(params) {
//		var url = params.url;
//		var ret = { };
//		if (url) {
//			if (url.indexOf("jquery") > 0) {
//				ret.redirectUrl = chrome.extension.getURL("script/jquery.min.js");
//			}
//		}
		return {};
	} },
	{ "site": "sina.com", "cata": "news" },
	{ "site": "hi.baidu.com", "cata": "blog" },
	{ "site": "baidu.com", "cata": "search" },
	{ "site": "mail.google.com", "cata": "office" },
	{ "site": "google.cn", "cata": "search" },
	{ "site": "google.com", "cata": "search" },
	{ "site": "mail.richinfo.cn", "cata": "office" },
	{ "site": "blog.zheezes.com", "cata": "blog" },
	{ "site": "csdn.net", "cata": "IT" },
	{ "site": "w3.org", "cata": "IT" },
	{ "site": "open.chrome.360.cn", "cata": "IT" },
	{ "site": "cqnnn.com", "cata": "finance" },
	{ "site": "meitu.com", "cata": "tool" },
	{ "site": "dict.youdao.com", "cata": "learn" },
	{ "site": "youku.com", "cata": "media" },
	{ "site": "douban.fm", "cata": "media" },
	{ "site": "cnn.com", "cata": "news" },
	{ "site": "foxnews.com", "cata": "news" },
	{ "site": "360doc.com", "cata": "news", "relieve": function() {
		$.cookie("LoginName", "fuck_360");
	} }
];

function getSiteEntry(site) {
	for (var i = 0; i < g_sites.length; i++) {
		var entry = g_sites[i];
		if (site.indexOf(entry.site) < 0) {
			continue;
		}
		return entry;
	}
	return null;
}

function findSiteCate(site) {
	var entry = getSiteEntry(site);
	if (entry) {
		return entry.cata;
	}
	return "unknown";
}
